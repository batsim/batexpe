package batexpe

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"
)

const (
	SUCCESS int = iota
	TIMEOUT
	FAILURE
)

type CmdFinishedMsg struct {
	Name  string
	State int
}

func PrepareDirs(exp Experiment) error {
	// Create output directory if needed
	outErr := CreateDirIfNeeded(exp.OutputDir)
	logErr := CreateDirIfNeeded(exp.OutputDir + "/log")
	cmdErr := CreateDirIfNeeded(exp.OutputDir + "/cmd")

	if (outErr != nil) || (logErr != nil) || (cmdErr != nil) {
		log.WithFields(log.Fields{
			"output-dir":     exp.OutputDir,
			"output-dir_err": outErr,
			"log-dir":        exp.OutputDir + "/log",
			"log-dir_err":    logErr,
			"cmd-dir":        exp.OutputDir + "/cmd",
			"cmd-dir_err":    cmdErr,
		}).Error("Cannot create output directory")
		return fmt.Errorf("Cannot create output directory")
	}

	return nil
}

func waitReadyForSimulation(exp Experiment, batargs BatsimArgs) error {
	log.WithFields(log.Fields{
		"ready timeout (seconds)":   exp.ReadyTimeout,
		"extracted socket endpoint": batargs.Socket,
		"batsim command":            exp.Batcmd,
	}).Info("Waiting for valid context")

	if strings.HasPrefix(batargs.Socket, "tcp") {
		port, err := PortFromBatSock(batargs.Socket)
		if err != nil {
			log.WithFields(log.Fields{
				"err":                       err,
				"extracted socket endpoint": batargs.Socket,
				"batsim command":            exp.Batcmd,
			}).Error("Cannot retrieve port from Batsim socket")
			return err
		}

		socketInUse := true
		anotherBatsim := true

		sockChan := make(chan int)
		batChan := make(chan int)

		go waitTcpPortAvailableSs(port, sockChan)
		go waitNoConflictingBatsim(batargs, batChan)

		for socketInUse || anotherBatsim {
			select {
			case <-time.After(time.Duration(exp.ReadyTimeout) * time.Second):
				log.WithFields(log.Fields{
					"ready timeout (seconds)":    exp.ReadyTimeout,
					"scanned port":               port,
					"batsim command":             exp.Batcmd,
					"socket in use":              socketInUse,
					"conflicting batsim running": anotherBatsim,
				}).Error("Context remains invalid")
				return fmt.Errorf("Context remains invalid")
			case code := <-sockChan:
				if code == 0 {
					socketInUse = false
				} else {
					return fmt.Errorf("Could not determine whether the network port is in use")
				}
			case code := <-batChan:
				if code == 0 {
					anotherBatsim = false
				} else {
					return fmt.Errorf("Could not determine whether other Batsim instances are running")
				}
			}
		}
	} else if strings.HasPrefix(batargs.Socket, "ipc") {
		// TODO check what can be checked on the ipc path endpoint
	}
	return nil
}

func areConflictingBatsimInstances(batCtx1, batCtx2 BatsimArgs) bool {
	port1, err1 := PortFromBatSock(batCtx1.Socket)
	port2, err2 := PortFromBatSock(batCtx2.Socket)

	if (err1 != nil) && (err2 != nil) {
		if port1 == port2 {
			return true
		}
	}

	return false
}

func waitNoConflictingBatsim(batargsToLaunch BatsimArgs, onexit chan int) {
	r := regexp.MustCompile(`(?m)^\S*\bbatsim\s+.*$`)
	for {
		// Retrieve running Batsim processes
		psCmd := exec.Command("ps")
		psCmd.Args = []string{psCmd.Args[0], "-e", "-o", "command"}

		outBuf, err := psCmd.Output()
		if err != nil {
			log.WithFields(log.Fields{
				"err":     err,
				"command": psCmd,
			}).Error("Cannot list running processes via ps")
			onexit <- 1
			return
		}

		conflict := false
		for _, batcmd := range r.FindAllString(string(outBuf), -1) {
			if !strings.Contains(batcmd, "--dump-execution-context") {
				log.WithFields(log.Fields{
					"batcmd": batcmd,
				}).Debug("Found a running batsim")

				batargs, parseErr := ParseBatsimCommand(batcmd)
				if parseErr == nil {
					conflictHere := areConflictingBatsimInstances(batargsToLaunch, batargs)
					if conflictHere {
						log.WithFields(log.Fields{
							"instance to launch": batargsToLaunch,
							"running instance":   batargs,
						}).Debug("Conflict with a running instance")

						conflict = true
					}
				}
			}
		}

		if !conflict {
			onexit <- 0
			return
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func waitTcpPortAvailableSs(port uint16, onexit chan int) {
	portStr := strconv.FormatUint(uint64(port), 10)
	r := regexp.MustCompile(":" + portStr)

	for {
		ssCmd := exec.Command("ss")
		ssCmd.Args = []string{ssCmd.Args[0], "-tln"}

		outBuf, err := ssCmd.Output()
		if err != nil {
			log.WithFields(log.Fields{
				"err":     err,
				"command": ssCmd,
			}).Error("Cannot list open sockets via ss")
			onexit <- 1
			return
		}

		if !(r.Match(outBuf)) {
			onexit <- 0
			return
		} else {
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func logExecuteTimeoutError(errMsg string, err error,
	name, cmdString, cmdFile, stdoutFile, stderrFile string,
	cmd *exec.Cmd, timeout float64, previewOnError bool) {

	log.WithFields(log.Fields{
		"process name":                 name,
		"err":                          err,
		"command":                      cmdString,
		"command file":                 cmdFile,
		"stdout file":                  stdoutFile,
		"stderr file":                  stderrFile,
		"simulation timeout (seconds)": timeout,
	}).Error(errMsg)

	// If the option is set, preview simulation logs to stderr
	if previewOnError {
		var linesToPreview int64 = 20
		// Preview stdout log unless set to /dev/null (batsim process)
		if stdoutFile != "/dev/null" {
			outPreview, err := PreviewFile(stdoutFile, linesToPreview)
			if err == nil {
				if outPreview != "" {
					fmt.Fprintf(os.Stderr,
						"\nContent of %s's stdout log:\n%s\n",
						name, outPreview)
				}
			} else {
				log.WithFields(log.Fields{
					"process name": name,
					"err":          err,
					"command":      cmdString,
					"command file": cmdFile,
					"stdout file":  stdoutFile,
				}).Error("Cannot read stdout file")
			}
		}

		// Preview stderr
		errPreview, err := PreviewFile(stderrFile, linesToPreview)
		if err == nil {
			if errPreview != "" {
				fmt.Fprintf(os.Stderr, "\nContent of %s's stderr log:\n%s\n",
					name, errPreview)
			}
		} else {
			log.WithFields(log.Fields{
				"process name": name,
				"err":          err,
				"command":      cmdString,
				"command file": cmdFile,
				"stderr file":  stderrFile,
			}).Error("Cannot read stderr file")
		}
	}
}

// Execute a command, writing status result on a channel
func ExecuteTimeout(name, cmdString, cmdFile, stdoutFile, stderrFile,
	subprocessType string,
	cmd *exec.Cmd, timeout float64, onstart chan CmdFinishedMsg,
	onexit chan CmdFinishedMsg, previewOnError bool) {

	log.WithFields(log.Fields{
		"process name": name,
		"command":      cmdString,
		"command file": cmdFile,
		"timeout":      timeout,
	}).Debug(fmt.Sprintf("Starting %s subprocess", subprocessType))

	if err := cmd.Start(); err != nil {
		// Start failed
		log.WithFields(log.Fields{
			"process name": name,
			"command":      cmdString,
			"command file": cmdFile,
			"stdout file":  stdoutFile,
			"stderr file":  stderrFile,
		}).Error(fmt.Sprintf("Could not start %s subprocess",
			subprocessType))
		onstart <- CmdFinishedMsg{name, FAILURE}
		onexit <- CmdFinishedMsg{name, FAILURE}
		return
	}

	// Start succeeded
	pid := cmd.Process.Pid
	done := make(chan error, 1)
	go func() {
		done <- cmd.Wait()
	}()
	onstart <- CmdFinishedMsg{name, SUCCESS}

	// Wait until command completion (or context timeout)
	select {
	case <-time.After(time.Duration(timeout) * time.Second):
		logExecuteTimeoutError(
			fmt.Sprintf("%s subprocess failed (simulation timeout reached)",
				subprocessType), nil,
			name, cmdString, cmdFile, stdoutFile, stderrFile, cmd, timeout,
			previewOnError)
		KillProcess(pid)
		onexit <- CmdFinishedMsg{name, TIMEOUT}
	case err := <-done:
		if err != nil {
			logExecuteTimeoutError(
				fmt.Sprintf("%s subprocess failed", subprocessType), err,
				name, cmdString, cmdFile, stdoutFile, stderrFile, cmd, timeout,
				previewOnError)
			KillProcess(pid)
			onexit <- CmdFinishedMsg{name, FAILURE}
		} else {
			log.WithFields(log.Fields{
				"process name": name,
				"command":      cmdString,
				"command file": cmdFile,
				"stdout file":  stdoutFile,
				"stderr file":  stderrFile,
			}).Info(fmt.Sprintf("%s subprocess succeeded", subprocessType))
			onexit <- CmdFinishedMsg{name, SUCCESS}
		}
	}
}

// "Batsim" <-> "Scheduler"
func oppName(str string) string {
	if str == "Batsim" {
		return "Scheduler"
	} else {
		return "Batsim"
	}
}

func cleanupSubprocesses(pidsToKill map[string]int) {
	for name, pid := range pidsToKill {
		log.WithFields(log.Fields{
			"name": name,
			"pid":  pid,
		}).Warn("Killing process")
		KillProcess(pid)
	}
}

func setupGuards(pidsToKill *map[string]int, onAbort chan int) {
	// Guard against ctrl+c
	// Does not seem needed, as SIGTERM is also launched in this case

	// Guard against (polite) kill
	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sigterm
		log.Warn("SIGTERM received. Killing remaining subprocesses.")
		cleanupSubprocesses(*pidsToKill)
		onAbort <- 3
	}()
}

func executeBatsimAlone(exp Experiment, previewOnError bool) int {
	log.WithFields(log.Fields{
		"simulation timeout (seconds)": exp.SimulationTimeout,
		"batsim command":               exp.Batcmd,
		"batsim cmdfile":               exp.OutputDir + "/cmd/batsim.bash",
		"batsim logfile":               exp.OutputDir + "/log/batsim.log",
	}).Info("Starting simulation")

	// Create command
	cmd := exec.Command("bash")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true} // To kill subprocesses later on
	cmd.Args = []string{cmd.Args[0], "-eux",
		exp.OutputDir + "/cmd/batsim.bash"}

	// Create files
	createBatsimCmdErr := ioutil.WriteFile(exp.OutputDir+"/cmd/batsim.bash",
		[]byte(exp.Batcmd), 0755)
	batlog, createBatsimLogErr := os.Create(exp.OutputDir + "/log/batsim.log")

	if createBatsimLogErr == nil {
		defer batlog.Close()
		cmd.Stderr = batlog
	}

	if (createBatsimCmdErr != nil) || (createBatsimLogErr != nil) {
		log.WithFields(log.Fields{
			"batsim cmdfile":     exp.OutputDir + "/cmd/batsim.bash",
			"batsim cmdfile err": createBatsimCmdErr,
			"batsim logfile":     exp.OutputDir + "log/batsim.log",
			"batsim logfile err": createBatsimLogErr,
		}).Error("Cannot create file")
		return 1
	}

	// Guards against SIGINT (ctrl+c) and SIGTERM (polite kill)
	pidsToKill := make(map[string]int)
	abort := make(chan int)
	setupGuards(&pidsToKill, abort)

	// Execute the process
	start := make(chan CmdFinishedMsg)
	termination := make(chan CmdFinishedMsg)
	go ExecuteTimeout("Batsim", exp.Batcmd, exp.OutputDir+"/cmd/batsim.bash",
		"/dev/null", exp.OutputDir+"/log/batsim.log", "Simulation", cmd,
		exp.SimulationTimeout, start, termination, previewOnError)

	start1 := <-start
	if start1.State == SUCCESS {
		pidsToKill["Batsim"] = cmd.Process.Pid
	}

	select {
	case finish1 := <-termination:
		delete(pidsToKill, "Batsim")
		return finish1.State
	case abortCode := <-abort:
		return abortCode
	}
}

func executeBatsimAndSched(exp Experiment, previewOnError bool) int {
	log.WithFields(log.Fields{
		"simulation timeout (seconds)": exp.SimulationTimeout,
		"batsim command":               exp.Batcmd,
		"batsim cmdfile":               exp.OutputDir + "/cmd/batsim.bash",
		"batsim logfile":               exp.OutputDir + "/log/batsim.log",
		"scheduler command":            exp.Schedcmd,
		"scheduler cmdfile":            exp.OutputDir + "/cmd/sched.bash",
		"scheduler logfile (out)":      exp.OutputDir + "/log/sched.out.log",
		"scheduler logfile (err)":      exp.OutputDir + "/log/sched.err.log",
	}).Info("Starting simulation")

	// Create commands
	cmds := make(map[string]*exec.Cmd)
	success := make(map[string]int)

	cmds["Batsim"] = exec.Command("bash")
	cmds["Batsim"].SysProcAttr = &syscall.SysProcAttr{Setpgid: true} // To kill subprocesses later on
	cmds["Batsim"].Args = []string{cmds["Batsim"].Args[0], "-eux",
		exp.OutputDir + "/cmd/batsim.bash"}

	cmds["Scheduler"] = exec.Command("bash")
	cmds["Scheduler"].SysProcAttr = &syscall.SysProcAttr{Setpgid: true} // To kill subprocesses later on
	cmds["Scheduler"].Args = []string{cmds["Scheduler"].Args[0], "-eux",
		exp.OutputDir + "/cmd/sched.bash"}

	// Create files
	createBatsimCmdErr := ioutil.WriteFile(exp.OutputDir+"/cmd/batsim.bash",
		[]byte(exp.Batcmd), 0755)
	batlog, createBatsimLogErr := os.Create(exp.OutputDir + "/log/batsim.log")
	createSchedCmdErr := ioutil.WriteFile(exp.OutputDir+"/cmd/sched.bash",
		[]byte(exp.Schedcmd), 0755)
	schedout, createSchedLogErr := os.Create(exp.OutputDir +
		"/log/sched.out.log")
	schederr, createSchedErrErr := os.Create(exp.OutputDir +
		"/log/sched.err.log")

	if createBatsimLogErr == nil {
		defer batlog.Close()
		cmds["Batsim"].Stderr = batlog
	}

	if createSchedLogErr == nil {
		defer schedout.Close()
		cmds["Scheduler"].Stdout = schedout
	}

	if createSchedErrErr == nil {
		defer schederr.Close()
		cmds["Scheduler"].Stderr = schederr
	}

	if (createBatsimCmdErr != nil) || (createSchedCmdErr != nil) ||
		(createBatsimLogErr != nil) ||
		(createSchedLogErr != nil) || (createSchedErrErr != nil) {
		log.WithFields(log.Fields{
			"batsim cmdfile":              exp.OutputDir + "/cmd/batsim.bash",
			"batsim cmdfile err":          createBatsimCmdErr,
			"batsim logfile":              exp.OutputDir + "log/batsim.log",
			"batsim logfile err":          createBatsimLogErr,
			"scheduler cmdfile":           exp.OutputDir + "/cmd/sched.bash",
			"scheduler cmdfile err":       createSchedCmdErr,
			"scheduler logfile (out)":     exp.OutputDir + "/log/sched.out.log",
			"scheduler logfile (out) err": createSchedLogErr,
			"scheduler logfile (err)":     exp.OutputDir + "/log/sched.err.log",
			"scheduler logfile (err) err": createSchedErrErr,
		}).Error("Cannot create file")
		return 1
	}

	// Guards against SIGINT (ctrl+c) and SIGTERM (polite kill)
	pidsToKill := make(map[string]int)
	abort := make(chan int)
	setupGuards(&pidsToKill, abort)

	// Execute the processes
	start := make(chan CmdFinishedMsg)
	termination := make(chan CmdFinishedMsg)
	go ExecuteTimeout("Batsim", exp.Batcmd, exp.OutputDir+"/cmd/batsim.bash",
		"/dev/null", exp.OutputDir+"/log/batsim.log", "Simulation",
		cmds["Batsim"], exp.SimulationTimeout, start, termination,
		previewOnError)
	go ExecuteTimeout("Scheduler", exp.Schedcmd,
		exp.OutputDir+"/cmd/sched.bash",
		exp.OutputDir+"/log/sched.out.log", exp.OutputDir+"/log/sched.err.log",
		"Simulation", cmds["Scheduler"], exp.SimulationTimeout, start,
		termination, previewOnError)

	// Wait for both to start (or to fail starting)
	nbStartedOrFailedStarting := 0
	for nbStartedOrFailedStarting < 2 {
		select {
		case start1 := <-start:
			if start1.State == SUCCESS {
				pidsToKill[start1.Name] = cmds[start1.Name].Process.Pid
			}
			nbStartedOrFailedStarting += 1
		}
	}

	// Wait for first process to finish
	var finish1, finish2 CmdFinishedMsg
	finish1 = <-termination
	delete(pidsToKill, finish1.Name)
	success[finish1.Name] = finish1.State

	log.WithFields(log.Fields{
		"name":  finish1.Name,
		"state": finish1.State,
	}).Debug("First process finished")

	// Depending on the first process success state, we'll wait the second
	// process differently.
	switch finish1.State {
	case SUCCESS:
		log.WithFields(log.Fields{
			"success timeout (seconds)": exp.SuccessTimeout,
			"potential victim name":     oppName(finish1.Name),
		}).Info("The second process might be killed soon...")

		select {
		case <-time.After(time.Duration(exp.SuccessTimeout) * time.Second):
			// Success timeout reached
			log.WithFields(log.Fields{
				"success timeout (seconds)": exp.SuccessTimeout,
			}).Warn("Success timeout reached")

			// Kill the other process
			KillProcess(pidsToKill[oppName(finish1.Name)])
			finish2 = <-termination
		case finish2 = <-termination:
		}
	case FAILURE:
		log.WithFields(log.Fields{
			"failure timeout (seconds)": exp.FailureTimeout,
			"potential victim name":     oppName(finish1.Name),
		}).Info("The second process might be killed soon...")

		select {
		case <-time.After(time.Duration(exp.FailureTimeout) * time.Second):
			// Failure timeout reached
			log.WithFields(log.Fields{
				"failure timeout (seconds)": exp.FailureTimeout,
			}).Warn("Failure timeout reached")

			// Kill the other process
			KillProcess(pidsToKill[oppName(finish1.Name)])
			finish2 = <-termination
		case finish2 = <-termination:
		}
	case TIMEOUT:
		// Wait second process completion
		finish2 = <-termination
	}

	// Second process finished
	delete(pidsToKill, finish2.Name)
	success[finish2.Name] = finish2.State

	select {
	case <-time.After(time.Duration(1) * time.Millisecond):
		return max(success["Batsim"], success["Scheduler"])
	case abortCode := <-abort:
		return abortCode
	}
}

// Execute one Batsim simulation
func ExecuteOne(exp Experiment, previewOnError bool) int {
	// Prepare execution
	err := PrepareDirs(exp)
	if err != nil {
		return 1
	}

	// Sets unset command as empty string
	if exp.Schedcmd == "schedcmd-unset" {
		exp.Schedcmd = ""
	}

	// Parse batsim command
	batargs, err := ParseBatsimCommand(exp.Batcmd)
	if err != nil {
		log.WithFields(log.Fields{
			"command": exp.Batcmd,
			"err":     err,
		}).Error("Cannot retrieve information from Batsim command")
		return 1
	}

	if !strings.HasPrefix(batargs.ExportPrefix, exp.OutputDir) {
		log.WithFields(log.Fields{
			"batsim prefix":    batargs.ExportPrefix,
			"output directory": exp.OutputDir,
			"batsim command":   exp.Batcmd,
		}).Warning("Batsim export prefix mismatches output directory")
	}

	if exp.Schedcmd == "" {
		// Only execute Batsim
		if batargs.BatexecMode == false {
			log.WithFields(log.Fields{
				"batsim command":    exp.Batcmd,
				"scheduler command": exp.Schedcmd,
			}).Error("Sched command unset but Batsim is not in batexec mode")
			return 1
		}

		return executeBatsimAlone(exp, previewOnError)
	} else {
		// Execute Batsim and the scheduler
		if batargs.BatexecMode == true {
			log.WithFields(log.Fields{
				"batsim command":    exp.Batcmd,
				"scheduler command": exp.Schedcmd,
			}).Error("Sched command set but Batsim is in batexec mode")
			return 1
		}

		// Wait for context to be ready (open sockets, batsim processes...)
		err := waitReadyForSimulation(exp, batargs)
		if err != nil {
			return 1
		}

		return executeBatsimAndSched(exp, previewOnError)
	}
}

func KillProcess(pid int) {
	syscall.Kill(-pid, syscall.SIGTERM)
}
