{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/master.tar.gz")
  {}
}:

let
  self = rec {
    batsim = kapack.batsim;
    shell = kapack.pkgs.mkShell {
      name = "shell";
      buildInputs = [
        kapack.pkgs.go

        kapack.pkgs.coreutils
        kapack.pkgs.procps
        kapack.pkgs.iproute

        kapack.pkgs.bats
        kapack.pkgs.python3
        kapack.pkgs.which

        batsim
        kapack.batsched
        kapack.yamldiff
        kapack.gocov
        kapack.gocovmerge
      ];
    };
  };
in
  self
