module framagit.org/batsim/batexpe

go 1.16

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/ghodss/yaml v1.0.0
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
