VERSION=`git describe --dirty`
LDFLAGS=-ldflags "-X main.version=${VERSION}"

build: ./bin/robin ./bin/robin.cover ./bin/robintest ./bin/robintest.cover

./bin/robin:
	(cd ./cmd/robin && go build ${LDFLAGS} -o ../../$@ .)

./bin/robin.cover:
	(cd ./cmd/robin && go test -c -o ../../$@ -covermode=count -coverpkg=../../,../robin/,../robintest/ .)

./bin/robintest:
	(cd ./cmd/robintest && go build ${LDFLAGS} -o ../../$@ .)

./bin/robintest.cover:
	(cd ./cmd/robintest && go test -c -o ../../$@ -covermode=count -coverpkg=../../,../robin/,../robintest/ .)

clean:
	rm -f ./bin/robin
	rm -f ./bin/robin.cover
	rm -f ./bin/robintest
	rm -f ./bin/robintest.cover
